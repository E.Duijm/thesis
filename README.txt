Readme for Thesis project by Erik Duijm
Project to detect spatial-temporal outliers in energy generation data.
We use the MDI algorithm to detect temporal outliers. These are investigated further using the SLOM algorithm.

For questions regarding this project, feel free to contact me at erik.duijm@gmail.com

# MDI
The MDI folder contains the libmaxdiv folder. This contains the implementation from the original authors as found on https://github.com/cvjena/libmaxdiv
Contains minor changes (small bug fix and added setting of spatial context embedding parameters in the python wrapper)
MDI\libmaxdiv\maxdiv\run_mdi.py is added by me and the main change with the original.
It is used to reshape the xarray data, communicate with the c++ implementation and save the output.

To run the MDI algorithm, change the run_mdi.py file such that the parameters are suited for your needs, and the data paths point to your xarray data
Then simply run the run_mdi.py file a suitable environment

# SLOM
SLOM algorithm uses cython implementation.
The algorithm, and the postprocessing are performed by the cython_slom.pyx file. 
After making changes to cython_slom.pyx ensure that you build the cython code.
To build the cython file ensure that you are in the folder containing the cython files and run the following command:

python setup.py build_ext --inplace

After succesfully building the cython_slom.pyx you can import it as a library for python
This can be done using the run_calcslom.py file. Change variables such as input and output file names and postprocessing threshold.
The path to data to investigate using SLOM is set in the cython_slom.pyx file. Change your data source there.



# Note on the jupyter notebooks:
The project contains three jupyter notebooks.
The Clustering_regions.ipynb is used for cluster detection to highlight potential interesting regions for MDI
plotting.ipynb contains code used to create figures
data_exploration.ipynb is the notebook used for initial exploration of the data, as well as investigating data distributions and seasonal behavior

# test_data contains several xarray data sets used during the project

# result figures contain figures of the results discussed in the Thesis