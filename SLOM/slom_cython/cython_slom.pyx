# cython: infer_types=True
# cython: boundscheck=False
# cython: wraparound=False
# cython: cdivision=True
# cython: language_level=3

import numpy as np
import pandas as pd
import xarray as xr
import netCDF4
import time as timer
from cython.parallel import prange
from libc.math cimport sqrt, fabs


#cython type definitions:
cdef int coarse_factor, X, Y
cdef double south_bound, north_bound, west_bound, east_bound

#PARAMETERS
filenameNL = "ERA5-NL_TenneT-CF_2018.nc"
filenameERA5EU = "ERA5-EU_t2m_201907.nc"
filenameERA5EU_stager = "ERA5-EU/ERA5-EU_Erik-CF_1987.nc"
file_70yearsummed = "ERA5-EU_EV_Zuijlen_summed/ERA5-EU_EV-Zuijlen_1950-2019_THS_Zscores_fixed.nc"
file_paper = "paper_data.nc"
file_name = file_paper

data_path_desktop = "C:\\Users\\Erik\\Documents\\Uni\\Thesis\\scripts\\test_data\\"
data_path_laptop = "C:\\Users\\erikd\\Documents\\Uni\\Thesis\\Code\\thesis\\test_data\\"
data_path_remote = "~/Documents/Thesis/code/thesis/test_data/"
data_stager2_path = "/media/DataStager2/"
data_gate3_path = "/media/DataGate3/"
data_path = data_path_remote

fileglob = data_gate3_path + "ERA5-EU_EV-Zuijlen/ERA5*.nc"

#List to store outlier events
assigned_to_event = []

#initialize event of single coordinate 
slom_events = []

#Coarsen factor
coarse_factor = 1
time_chunks = "auto"


#Boundary params
neighbor_coord_shift =[(-1,-1),(-1,0),(-1,1),(1,-1),(1,0),(1,1),(0,-1),(0,1)]
#south_bound = 35
#north_bound = 74.75
#west_bound = -14.75
#east_bound = 40

south_bound = 53.75
north_bound = 57.5
west_bound = 4.75
east_bound = 8

### LAT LON BOUNDARIES
#SET UP FOR EU DATA CURRENTLY
lat_slice = slice(south_bound,north_bound)
lon_slice = slice(west_bound,east_bound)

class outlier_event():
    
    #Ensure that timestep is datetime64, and coordslist is a list of tripples.
    def __init__(self, timestep, coordslist):
        self.event = [(timestep, coordslist)]
    
    def extend_event(self, timestep, coordslist):
        #Extend event, if timestep exists in event, only extend lats lons slom triple
        #there should only be at most one match, as there are no duplicate timesteps allowed
        match = [event_slice for event_slice in self.event if timestep in event_slice]
        
        #Check if there are 1 or 0 matches
        if len(match) == 0:
            self.event.append((timestep, coordslist))
        else:
            for coord_triple in coordslist:
                if coord_triple not in match[0][1]:
                    match[0][1].append(coord_triple)


########################################################## SLOM CALCULATION

cdef float calculate_beta(int x, int y, float[:,:] data) nogil:
    cdef Py_ssize_t x_max = data.shape[0]
    cdef Py_ssize_t y_max = data.shape[1]
    cdef Py_ssize_t nx  
    cdef Py_ssize_t ny
    cdef float i,
    cdef float beta, d, objectval, val, avg_d
    
    beta=0
    d=0
    i=0
    objectval = data[x,y]
    for nx in range(-1,2):
        for ny in range(-1,2):
            if not (nx==0 and ny==0):
                if x + nx >= 0 and x+nx<x_max and y+ny>=0 and y+ny<y_max:
                    val = data[x + nx,y+ny]
                    d+=val
                    i+=1
                    
    avg_d=(d+objectval)/(i+1)
    
    for nx in range(-1,2):
        for ny in range(-1,2):
            #we omit the checking for 0,0 as the object itself adds to beta
                if x + nx >= 0 and x+nx<x_max and y+ny>=0 and y+ny<y_max:
                    if(data[x+nx,y+ny]>avg_d):
                        beta+=1
                    if(data[x+nx,y+ny]<avg_d):
                        beta-=1

    beta=fabs(beta)
    beta=max(beta,1)/(i-1)
    beta=beta/(1+(d/i))
    return beta

#data is xarray 

#todo, store slom results seperately from data, as SLOM check is removed, since data is now np.ndarray instead of xarray
cdef float multivar_calculate_dtilde(int x, int y, float[:,:,:] data) nogil:
    cdef Py_ssize_t x_max = data.shape[0]
    cdef Py_ssize_t y_max = data.shape[1]
    cdef Py_ssize_t v_max = data.shape[2]
    cdef Py_ssize_t nx  
    cdef Py_ssize_t ny
    cdef Py_ssize_t v
    cdef float[:] objectval, neighborval
    cdef float i
    cdef float d, maxd, dist
    d=0
    i=0
    objectval = data[x,y]
    maxd=-10000
    for nx in range(-1,2):
        for ny in range(-1,2):
            if not (nx==0 and ny==0):
                if x + nx >= 0 and x+nx<x_max and y+ny>=0 and y+ny<y_max:
                    neighborval = data[x+nx,y+ny]
                    dist=0
                    for v in range(v_max):
                        dist+=(neighborval[v]-objectval[v])**2
                    dist = sqrt(dist)
                    if dist > maxd:
                        maxd = dist
                    d+=dist
                    i+=1

    return (d-maxd)/(i-1)

cdef float[:,:] slom_per_timeslice(float[:,:,:] timeslice) nogil:
    #ensure timeslice input data is normalized!
    cdef Py_ssize_t x_max = timeslice.shape[0]
    cdef Py_ssize_t y_max = timeslice.shape[1]
    cdef Py_ssize_t v_max = timeslice.shape[2]
    cdef Py_ssize_t x, y 
    
    with gil:
        s = np.zeros((x_max, y_max), dtype = np.float32)
        dt = np.zeros((x_max, y_max), dtype = np.float32)
        b = np.zeros((x_max, y_max), dtype = np.float32)

    #this is not allowed, currently parallel is not working, sad as heck
    cdef float[:,:] slom_array = s
    cdef float[:,:] d_tilde_array = dt
    cdef float[:,:] beta_array = b
#     cdef np.ndarray[double, ndim=2] d_tilde_array = np.zeros((x_max, y_max))
#     cdef np.ndarray[double, ndim=2] beta_array = np.zeros((x_max, y_max))
#     cdef np.ndarray[double, ndim=2] slom_array = np.zeros((x_max, y_max))
    #Calculate d tilde, and save in d_tilde_array    

    for x in range(x_max):
        for y in range(y_max):
            d_tilde_array[x,y]=multivar_calculate_dtilde(x,y,timeslice)
    
    #
    for x in range(x_max):
        for y in range(y_max):
            beta_array[x,y]=calculate_beta(x,y,d_tilde_array)
    
    #
    for x in range(x_max):
        for y in range(y_max):
            slom_array[x,y] = beta_array[x,y] * d_tilde_array[x,y]
    
    return(slom_array)



################################################################### EXTEND DISCOVERED OUTLIIERS TO OUTLIER REGIONS
#TODO: documenation
#TODO: Discuss threshold for combining
def extend_outlier(data, out_event, timestep, outlier_lat, outlier_lon, time_start, time_stop, threshold=0.15, time_res = 1, spatial_res = 0.25):

    past_time = timestep - np.timedelta64(time_res,'h')
    
    if past_time < time_stop and past_time >= time_start:
        past_neighbor = data.loc[dict(time=past_time, lat=outlier_lat, lon=outlier_lon)]
        #print(f"past_neighbor neighbor: {past_time}, lat: {outlier_lat}, lon {outlier_lon}, SLOM: {float(past_neighbor['SLOM'].values)}, threshold: {threshold}")    
        if float(past_neighbor['SLOM'].values) >= threshold:
            if (past_time, outlier_lat, outlier_lon) not in assigned_to_event:
                #print("add past")
                #Note that an outlier has been added to an event, assign it to the assigned list
                assigned_to_event.append((past_time, outlier_lat, outlier_lon))
                #Extend the event with the related outlier
                out_event.extend_event(past_time,[(outlier_lat, outlier_lon, float(past_neighbor['SLOM'].values))])
                #If we find and merge an matching outlier to the event, continue looking in the past to possibly merge more
                #recursively call extend_outlier
                out_event = extend_outlier(data, out_event, past_time, outlier_lat, outlier_lon, time_start, time_stop, threshold, time_res, spatial_res)
    
    future_time = timestep + np.timedelta64(time_res,'h')
    
    if future_time <= time_stop and future_time > time_start:
        future_neighbor = data.loc[dict(time=future_time, lat=outlier_lat, lon=outlier_lon)]
        #print(f"future_neighbor neighbor: {future_time}, lat: {outlier_lat}, lon {outlier_lon}, SLOM: {float(future_neighbor['SLOM'].values)}, threshold: {threshold}")    
        if float(future_neighbor['SLOM'].values) >= threshold:
            if (future_time, outlier_lat, outlier_lon) not in assigned_to_event:
                #print("add_future")
                #Note that an outlier has been added to an event, assign it to the assigned list
                assigned_to_event.append((future_time, outlier_lat, outlier_lon))
                #Extend the event with the related outlier
                out_event.extend_event(future_time,[(outlier_lat, outlier_lon, float(future_neighbor['SLOM'].values))])
                #If we find and merge an matching outlier to the event, continue looking in the future to possibly merge more
                #recursively call extend_outlier
                out_event = extend_outlier(data, out_event, future_time, outlier_lat, outlier_lon, time_start, time_stop, threshold, time_res, spatial_res)
   
    #Loop over all posible neighbors
    
    for coord_shift in neighbor_coord_shift:
        
        neighbor_lat = outlier_lat + spatial_res * coord_shift[0]
        neighbor_lon = outlier_lon + spatial_res * coord_shift[1]
        
        if neighbor_lat >= south_bound and neighbor_lat <= north_bound and neighbor_lon >= west_bound and neighbor_lon <= east_bound:
            spat_neighbor = data.loc[dict(time=timestep , lat=neighbor_lat, lon=neighbor_lon)]
            if float(spat_neighbor['SLOM'].values) >= threshold:
                #print(f"spatial neighbor: {timestep}, lat: {neighbor_lat}, lon {neighbor_lon}, SLOM: {float(spat_neighbor['SLOM'].values)}, threshold: {threshold}")
            
                if (timestep, neighbor_lat, neighbor_lon) not in assigned_to_event:
                    #print("add_spatial")
                    #Note that an outlier has been added to an event, assign it to the assigned list
                    assigned_to_event.append((timestep, neighbor_lat, neighbor_lon))
                    #Extend the event with the related outlier
                    out_event.extend_event(timestep,[(neighbor_lat, neighbor_lon, float(spat_neighbor['SLOM'].values))])
                    #If we find and merge an matching outlier to the event, continue looking in the spatial neighbor to possibly merge more
                    #recursively call extend_outlier
                    out_event = extend_outlier(data, out_event, timestep, neighbor_lat, neighbor_lon, time_start, time_stop, threshold, time_res, spatial_res)
                    
    return_event = out_event
    return(return_event)

### Function takes a list of single location outliers. So not yet extended. These are then extended using the extend_outlier_past
###     and extend_outlier_future functions.
### Input_n can be smaller then 1, indicating that values in that quantile will be used to extend outliers
### if input_n > 1, the quantile of the top input_n slom values is used as threshold.
### To ensure non trivial outliers, 0.1 is the minimum slom threshold.
### Returns a list of outlier events that are not overlapping

def extend_found_single_outliers(slom_events,input_n, subselection, xrDS):
    ### Calculate SLOM threshold using the quantile function of xarray
    num_vals = int(subselection.count().values)
    top_n = input_n
    min_thres = 0.1
    cdef double threshold=0.9
    if top_n > 1:
        threshold = float(subselection.quantile((num_vals-top_n)/num_vals).values)
    elif top_n <= 1:
        threshold = float(subselection.quantile(top_n).values)
    print(f"threshold: {threshold}, input_n used: {input_n}")
    if threshold<min_thres:
        print("slom threshold to low. Please select a larger top n or provide a larger sample such that 2sd > 0.1, threshold set at 0.1")
        threshold = 0.1
   
    #Sorting isn't needed, as the merging is symmetrical and relies on a flat threshold. Starting at the high SLOM has the same result, 
    #As it would grow to the same event, as starting at a smaller outlier that overlaps with the largest slome score.
    
    #time boundary params
    time_start = np.datetime64(xrDS.time[0].values)
    time_stop = np.datetime64(xrDS.time[int(xrDS.time.count().values) - 1].values)

    #List to store resulting outliers
    resulting_outlier_events=[]
    resulting_outlier_singletons=[]    
    for oev in slom_events:
        ### First check if the event is already assigned to another event, if so remove from slom_events
        
        timestep, coordlist = oev.event[0]
        
        if (timestep, coordlist[0][0], coordlist[0][1]) not in assigned_to_event:

            ### When an event is extended, add the original event to the assigned_to_event list
            assigned_to_event.append((timestep, coordlist[0][0], coordlist[0][1]))
            ### extend the original
            print(oev.event[0])
            extended_outlier = extend_outlier(xrDS,oev,oev.event[0][0],oev.event[0][1][0][0],oev.event[0][1][0][1], spatial_res = 0.25 * coarse_factor, time_start=time_start, time_stop=time_stop, threshold = threshold)
            #print(extended_outlier.event)
            if len(extended_outlier.event) == 1:
                resulting_outlier_singletons.append(extended_outlier)
            else:
                resulting_outlier_events.append(extended_outlier)
        
    return(resulting_outlier_singletons, resulting_outlier_events, threshold)    

def calc_slom(outlier_timeslice = None, threshold = 0.95, univar=False, variable = "WON", slom_outlier_filename = None, result_file=None):

    #LOAD AND INITIALIZE DATA
    # SEE http://xarray.pydata.org/en/stable/dask.html?highlight=rechunk#chunking-and-performance FOR TIPS ON OPTIMIZATION
    print("loading data")
    startload = timer.perf_counter()
    #xrDS = xr.open_dataset(data_path+file_name)
    xrDS = xr.open_mfdataset(fileglob, concat_dim = 'time')
    #xrDS = xrDS.sel(lon=lon_slice,lat=lat_slice)
    if outlier_timeslice != None:
        xrDS = xrDS.isel(time=outlier_timeslice)
    print("precoarse data:")
    print(xrDS)
    xrDS = xrDS.sel(lat=lat_slice, lon=lon_slice)
    xrDS = xrDS.coarsen(lat = coarse_factor, boundary ="trim").mean().coarsen(lon = coarse_factor, boundary="trim").mean()

    latcount = int(xrDS.lat.count().values)
    loncount = int(xrDS.lon.count().values)
    if univar:
        xrDS[variable] = (xrDS[variable] - xrDS[variable].min())  / (xrDS[variable].max() - xrDS[variable].min())

    else:
        xrDS["WON"] = (xrDS["WON"] - xrDS["WON"].min())  / (xrDS["WON"].max() - xrDS["WON"].min())
        xrDS["WOF"] = (xrDS["WOF"] - xrDS["WOF"].min())  / (xrDS["WOF"].max() - xrDS["WOF"].min())
        xrDS["SPV"] = (xrDS["SPV"] - xrDS["SPV"].min())  / (xrDS["SPV"].max() - xrDS["SPV"].min())

    print("####     Dataset       ###")
    print(xrDS)

    stopload = timer.perf_counter()
    print(f"data loaded and regridded in {stopload - startload:0.2f}")



    ###################################################################END SLOM CALCULATION##
    ####### APPLY SLOM TO DATA
    start = timer.perf_counter()

    
    cdef float[:,:,:,:] npdata_view
    #transform dataset to stacked numpy array 
    if univar:
        data_vals = xrDS[variable].values
        data_vals = np.expand_dims(data_vals,3)
        npdata_view = data_vals

    else:
        data_won = xrDS.WON.values
        data_wof = xrDS.WOF.values
        data_spv = xrDS.SPV.values

        #create numpy array, save to memoryview
        npdata = np.stack((data_won,data_wof,data_spv),axis=3)
        npdata_view = npdata

    #Number of timesteps
    cdef int num_timesteps, num_lat, num_lon
    num_timesteps = int(xrDS.time.count())
    num_lat = int(xrDS.lat.count())
    num_lon = int(xrDS.lon.count())

    #Create array to store slom results
    result_nparray = np.zeros((num_timesteps, num_lat, num_lon), dtype = np.float32)
    cdef float[:,:,:] result_view = result_nparray

    cdef Py_ssize_t i

    #currently not parallel
    for i in range(num_timesteps):
        if i%8760==0:
            print(f"year: {i/8760}")
        result_view[i]=slom_per_timeslice(npdata_view[i,:,:,:])

    #load memory view into a result (might result_nparray also work?) 
    result = np.asarray(result_view) 

    xrDS["SLOM"]= xr.DataArray(data=result, coords=[xrDS.time, xrDS.lat, xrDS.lon])

    #Save the resulting slom scores:
    if slom_outlier_filename == None:
        slom_output_file = "temp_outlierfile.nc"
    else:   
        slom_output_file = slom_outlier_filename
    xrDS["SLOM"].to_netcdf(slom_output_file)

    #SELECT REGION BEING ANALYZED
    subselection = xrDS["SLOM"] #.sel(time=slice("01-01-2018"))
    #select the top n slom values
    top_n_selected = 10
    num_entries = int(subselection.count().values)
    #USE QUANTILE FUNCTION TO SELECT TOP X VALUES, ALL OTHER VALUES ARE NAN
    top_slom_nan = subselection.where(subselection>=float(subselection.quantile((num_entries-top_n_selected)/num_entries).values), drop=True)
    #STACK TOP_SLOM_NAN TO SAVE THE COORDS OF THE TOP VALUES
    stacked_slom = top_slom_nan.stack(x=['time','lat','lon'])
    #DROP ALL NAN
    top_slom = stacked_slom[stacked_slom.notnull()]

    #Fill a list with initially discovered outliers
    top_slom_list = []
    for top_slom_outlier in top_slom:
        time,lat,lon = top_slom_outlier.x.values.tolist()
        slom = float(top_slom_outlier.values)
        top_slom_list.append((time,lat,lon,slom))
      
    top_slom_list.sort(key= lambda tup: tup[3], reverse=True)
    for top_slom in top_slom_list:
        print(top_slom)
        
    for ts in top_slom_list:    
        event = outlier_event(ts[0],[(ts[1],ts[2],ts[3])])
        slom_events.append(event)

    #extend individual outliers
    extend_threshold = threshold
    result_size_1, resulte_larger_events, used_threshold =extend_found_single_outliers(slom_events,extend_threshold,subselection, xrDS)

    stop = timer.perf_counter()
    
    if result_file != None:
        output_file = "output/" + result_file
    else:
        output_file = "output/temp.log"
    with open(output_file,'w') as f:

        f.write(f"time spent on SLOM  {stop - start:0.2f} \n\n")
        f.write(f"File analyzed: {fileglob} \n\n")
        f.write(f"lat limits: {lat_slice}, lon limits: {lon_slice} \n\n")
        f.write(f"extension threshold set: {extend_threshold}\n\n")
        f.write(f"extension threshold used: {used_threshold}\n\n")
        
        f.write("events with size larger than 1: \n")
        for ev in resulte_larger_events:
            f.write(str(ev.event)+"\n")

        f.write("\n\n ###################################################################### \n \n")
        f.write("Singleton outliers: \n")
        for ev in result_size_1:
            f.write(str(ev.event)+"\n")

