# Install script for directory: /home/duijm/Documents/Thesis/code/thesis/pkgs/libmaxdiv/maxdiv/libmaxdiv/cmake/Modules/eigen-3.3.7/Eigen

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr/local")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

# Set default install directory permissions.
if(NOT DEFINED CMAKE_OBJDUMP)
  set(CMAKE_OBJDUMP "/usr/bin/objdump")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xDevelx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/eigen3/Eigen" TYPE FILE FILES
    "/home/duijm/Documents/Thesis/code/thesis/pkgs/libmaxdiv/maxdiv/libmaxdiv/cmake/Modules/eigen-3.3.7/Eigen/Cholesky"
    "/home/duijm/Documents/Thesis/code/thesis/pkgs/libmaxdiv/maxdiv/libmaxdiv/cmake/Modules/eigen-3.3.7/Eigen/CholmodSupport"
    "/home/duijm/Documents/Thesis/code/thesis/pkgs/libmaxdiv/maxdiv/libmaxdiv/cmake/Modules/eigen-3.3.7/Eigen/Core"
    "/home/duijm/Documents/Thesis/code/thesis/pkgs/libmaxdiv/maxdiv/libmaxdiv/cmake/Modules/eigen-3.3.7/Eigen/Dense"
    "/home/duijm/Documents/Thesis/code/thesis/pkgs/libmaxdiv/maxdiv/libmaxdiv/cmake/Modules/eigen-3.3.7/Eigen/Eigen"
    "/home/duijm/Documents/Thesis/code/thesis/pkgs/libmaxdiv/maxdiv/libmaxdiv/cmake/Modules/eigen-3.3.7/Eigen/Eigenvalues"
    "/home/duijm/Documents/Thesis/code/thesis/pkgs/libmaxdiv/maxdiv/libmaxdiv/cmake/Modules/eigen-3.3.7/Eigen/Geometry"
    "/home/duijm/Documents/Thesis/code/thesis/pkgs/libmaxdiv/maxdiv/libmaxdiv/cmake/Modules/eigen-3.3.7/Eigen/Householder"
    "/home/duijm/Documents/Thesis/code/thesis/pkgs/libmaxdiv/maxdiv/libmaxdiv/cmake/Modules/eigen-3.3.7/Eigen/IterativeLinearSolvers"
    "/home/duijm/Documents/Thesis/code/thesis/pkgs/libmaxdiv/maxdiv/libmaxdiv/cmake/Modules/eigen-3.3.7/Eigen/Jacobi"
    "/home/duijm/Documents/Thesis/code/thesis/pkgs/libmaxdiv/maxdiv/libmaxdiv/cmake/Modules/eigen-3.3.7/Eigen/LU"
    "/home/duijm/Documents/Thesis/code/thesis/pkgs/libmaxdiv/maxdiv/libmaxdiv/cmake/Modules/eigen-3.3.7/Eigen/MetisSupport"
    "/home/duijm/Documents/Thesis/code/thesis/pkgs/libmaxdiv/maxdiv/libmaxdiv/cmake/Modules/eigen-3.3.7/Eigen/OrderingMethods"
    "/home/duijm/Documents/Thesis/code/thesis/pkgs/libmaxdiv/maxdiv/libmaxdiv/cmake/Modules/eigen-3.3.7/Eigen/PaStiXSupport"
    "/home/duijm/Documents/Thesis/code/thesis/pkgs/libmaxdiv/maxdiv/libmaxdiv/cmake/Modules/eigen-3.3.7/Eigen/PardisoSupport"
    "/home/duijm/Documents/Thesis/code/thesis/pkgs/libmaxdiv/maxdiv/libmaxdiv/cmake/Modules/eigen-3.3.7/Eigen/QR"
    "/home/duijm/Documents/Thesis/code/thesis/pkgs/libmaxdiv/maxdiv/libmaxdiv/cmake/Modules/eigen-3.3.7/Eigen/QtAlignedMalloc"
    "/home/duijm/Documents/Thesis/code/thesis/pkgs/libmaxdiv/maxdiv/libmaxdiv/cmake/Modules/eigen-3.3.7/Eigen/SPQRSupport"
    "/home/duijm/Documents/Thesis/code/thesis/pkgs/libmaxdiv/maxdiv/libmaxdiv/cmake/Modules/eigen-3.3.7/Eigen/SVD"
    "/home/duijm/Documents/Thesis/code/thesis/pkgs/libmaxdiv/maxdiv/libmaxdiv/cmake/Modules/eigen-3.3.7/Eigen/Sparse"
    "/home/duijm/Documents/Thesis/code/thesis/pkgs/libmaxdiv/maxdiv/libmaxdiv/cmake/Modules/eigen-3.3.7/Eigen/SparseCholesky"
    "/home/duijm/Documents/Thesis/code/thesis/pkgs/libmaxdiv/maxdiv/libmaxdiv/cmake/Modules/eigen-3.3.7/Eigen/SparseCore"
    "/home/duijm/Documents/Thesis/code/thesis/pkgs/libmaxdiv/maxdiv/libmaxdiv/cmake/Modules/eigen-3.3.7/Eigen/SparseLU"
    "/home/duijm/Documents/Thesis/code/thesis/pkgs/libmaxdiv/maxdiv/libmaxdiv/cmake/Modules/eigen-3.3.7/Eigen/SparseQR"
    "/home/duijm/Documents/Thesis/code/thesis/pkgs/libmaxdiv/maxdiv/libmaxdiv/cmake/Modules/eigen-3.3.7/Eigen/StdDeque"
    "/home/duijm/Documents/Thesis/code/thesis/pkgs/libmaxdiv/maxdiv/libmaxdiv/cmake/Modules/eigen-3.3.7/Eigen/StdList"
    "/home/duijm/Documents/Thesis/code/thesis/pkgs/libmaxdiv/maxdiv/libmaxdiv/cmake/Modules/eigen-3.3.7/Eigen/StdVector"
    "/home/duijm/Documents/Thesis/code/thesis/pkgs/libmaxdiv/maxdiv/libmaxdiv/cmake/Modules/eigen-3.3.7/Eigen/SuperLUSupport"
    "/home/duijm/Documents/Thesis/code/thesis/pkgs/libmaxdiv/maxdiv/libmaxdiv/cmake/Modules/eigen-3.3.7/Eigen/UmfPackSupport"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xDevelx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/eigen3/Eigen" TYPE DIRECTORY FILES "/home/duijm/Documents/Thesis/code/thesis/pkgs/libmaxdiv/maxdiv/libmaxdiv/cmake/Modules/eigen-3.3.7/Eigen/src" FILES_MATCHING REGEX "/[^/]*\\.h$")
endif()

