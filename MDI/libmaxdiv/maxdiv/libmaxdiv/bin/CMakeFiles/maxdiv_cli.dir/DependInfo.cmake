# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/duijm/Documents/Thesis/code/thesis/pkgs/libmaxdiv/maxdiv/libmaxdiv/maxdiv.cc" "/home/duijm/Documents/Thesis/code/thesis/pkgs/libmaxdiv/maxdiv/libmaxdiv/bin/CMakeFiles/maxdiv_cli.dir/maxdiv.cc.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "MAXDIV_GAUSSIAN_CUMULATIVE_SIZE_LIMIT=2147483648"
  "MAXDIV_KDE_CUMULATIVE_SIZE_LIMIT=20000"
  "MAXDIV_NMP_LIMIT=10000"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/duijm/Documents/Thesis/code/thesis/pkgs/libmaxdiv/maxdiv/libmaxdiv/bin/CMakeFiles/maxdiv.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
