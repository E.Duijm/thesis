from setuptools import Extension, setup
from Cython.Build import cythonize

ext_modules = [
    Extension(
        "cython_slom",
        ["cython_slom.pyx"],
        extra_compile_args=['-/openmp'],
        extra_link_args=['-/openmp'],
    )
]


setup(
    ext_modules = cythonize("cython_slom.pyx", annotate=True)
)