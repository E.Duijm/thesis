#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 20 11:13:20 2021

@author: duijm
"""

import cython_slom as cs

timeslice = slice(415574, 415623) #Indicates what timesteps are outliers (according to MDI) to investigate further spatially
threshold = 0.95 #Accepts float as percentage or int as top N
univar = True
var = "WOF"
slom_outlier_filename = "output/slom_danwof_summed_CE_5_th095.nc" #To save the SLOM scores for future work
result_file = "slom_danwof_summed_CE_5_th095.result"

cs.calc_slom(outlier_timeslice=timeslice, threshold=threshold,univar=univar,variable=var,
             slom_outlier_filename=slom_outlier_filename, result_file=result_file)