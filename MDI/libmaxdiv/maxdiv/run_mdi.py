import numpy as np
import pandas as pd
import xarray as xr
import matplotlib.pyplot as plt
import netCDF4
import os
import maxdiv,libmaxdiv_wrapper
import cartopy.crs as ccrs
import cartopy.feature as cfeature
from libmaxdiv_wrapper import _search_libmaxdiv,maxdiv
import time

print(os.getcwd())

###################
#### FILE PATH AND FILE NAMES
data_path_remote = "~/Documents/Thesis/code/thesis/test_data/"
data_stager2_path = "/media/DataStager2/"
data_gate2_path = "/media/DataGate2/"
data_gate3_path = "/media/DataGate3/"
#data_path = data_path_laptop
#data_path = data_path_desktop
data_path = data_path_remote
filenameNL = "ERA5-NL_TenneT-CF_2018.nc"
filenameERA5EU = "ERA5-EU_t2m_201907.nc"
filenameBD = "ERA5-EU_CF_ErikBD.nc"
filenameERA5EU_stager = "ERA5-EU/ERA5-EU_Erik-CF_1987.nc"
filenameERA5EU_stagerNL_2000  ="ERA5-EU/ERA5-EU_Erik-CF_2000.nc"
filenameERA5EU_stagerNL_2001  ="ERA5-EU/ERA5-EU_Erik-CF_2001.nc"
fileglob = data_gate3_path + "ERA5-EU_EV-Zuijlen/TotalHourlySum/*.nc"
fileglob_danish_wof = data_gate3_path + "ERA5-EU_EV-Zuijlen/TotalHourlySum/danish_wof_cluster/*.nc"
file_70yearsummed = "ERA5-EU_EV_Zuijlen_summed/ERA5-EU_EV-Zuijlen_1950-2019_THS_Zscores_fixed.nc"
file_era5_zuijlen_sample = "ERA5-EU_EV-Zuijlen/ERA5-EU_EV-Zuijlen_201*.nc"
fileglob_spanish_spv = data_gate3_path + "ERA5-EU_EV-Zuijlen/TotalHourlySum/spanish_spv_cluster/*.nc"
fileglob_french_mixed = data_gate3_path + "ERA5-EU_EV-Zuijlen/TotalHourlySum/french_mixed_cluster/*.nc"
fileglob_german_won = data_gate3_path + "ERA5-EU_EV-Zuijlen/TotalHourlySum/german_won_cluster/*.nc"
fileglob_spattemp = data_gate3_path + "ERA5-EU_EV-Zuijlen/ERA5*.nc" 
filename =file_70yearsummed  

#INITIALISE XARRAY DATASET
#single file
#xrDS = xr.open_dataset(data_path+filename)

#multifile
#files = [data_path+filenameERA5EU_stagerNL_2000,data_path+filenameERA5EU_stagerNL_2001]
xrDS = xr.open_mfdataset(fileglob_spattemp, concat_dim = 'time')


south_bound = 35
north_bound = 74.75
west_bound = -14.75
east_bound = 40

### LAT LON BOUNDARIES
#SET UP FOR EU DATA CURRENTLY
lat_slice = slice(south_bound,north_bound)
lon_slice = slice(west_bound,east_bound)

time_slice =  slice("1950","2019")#slice("15-08-1987","18-08-1987")
#MANIPULATE RANGE SELECTION
data_timeslice = xrDS.sel(time=time_slice)
#remove feb 29th
#data_timeslice = data_timeslice.sel(time=~((data_timeslice.time.dt.month == 2) & (data_timeslice.time.dt.day == 29)))
#data_timeslice = data_timeslice.fillna(0)
data_small = data_timeslice.sel(lon=lon_slice,lat=lat_slice)

#Function that calls the mdi library with some paramaters for regridding the data
#Input params:
  #data: xarray input data
  #outputfile: filename of output
  #windowsize: takes an int, indicates the grid size of the coarsening. E.g. 2 means average data over a 2x2 window.
  #time_resolution: takes an int, indicates the time resolution over which to resample. Takes int that indicates the number of hours to be averaged into 1 timestep
  #time_only: bool. If True will turn spatial-temporal data into temporal data via averaging over lat lon
  #method, mode, proposals, preproc are as in the original mdi algorithm. See libmaxdiv/maxdiv/maxdiv.py for options
  #min_outlier_timesize and max_outlier_timesize: The min and max temporal length of outlier intervals
  #Univar: Bool, if True, Indicates only one variable will be used
  #variable: String, The variable name in the xarray data to use if univar is True
# 
# Function prepares the data (turns xarray into numpy array with correct dimensions)
# Calls the mdi algorithms C++ implementation via the libmaxdiv wrapper
# Returns an outputfile with top outliers

def calc_mdi(data,outputfile,windowsize=1,time_resolution=3, time_only = False, method = 'gaussian_cov', mode='TS', proposals ='dense', min_outlier_timesize=3, max_outlier_timesize = 72, univar=True, variable='t2m', preproc = None):
  
  t = time.localtime()
  current_time = time.strftime("%Y-%m-%d:%H:%M:%S", t)
  print(f"run started on:  {current_time}")
  print("#########################################################################################")
  if time_only:
    print(f"Starting MDI calculation for time only data and time resolution {time_resolution}.")
    
    #If data is already temporal, comment out below line, and comment the data_regrid = data.mean() line
    #data_regrid = data
    data_regrid = data.mean(dim=['lat','lon'])
    #data_regrid = data
    time_only_min_range = max(int(round(min_outlier_timesize/time_resolution)),1)
    time_only_max_range = max(int(round(max_outlier_timesize/time_resolution)),1) 

  else:    
    print(f"Starting MDI calculation for windowsize {windowsize} and time resolution {time_resolution}.")
    t = time.localtime()
    current_time = time.strftime("%Y-%m-%d:%H:%M:%S", t)
    print(f"run started on:  {current_time}")
  
    #regridding
    data_regrid = data.coarsen(lat = windowsize, boundary="trim").mean().coarsen(lon = windowsize, boundary="trim").mean()
    print("done spatial regrid")
    data_regrid = data_regrid.resample(time=f'{time_resolution}h').mean()
    print("temporal resample complete")
    #Target min size = 250 km. Regular grid cell size is approximately 25 km in ERA5 eu data
    #Target max size = 750 km. 


    #Since extint_min/max_len take ints that do not correspond directly to hours the time resolution needs to be converted. Take into account the regridding.
    #Thus if rescaled to 3h res, 4x4 grid size, extint_min_len [1,1,1,1] is size 3hours 100 x 100 km as minimum tensor size
    min_grid = max(int(round(10/windowsize)),1) #scale 10 (250km) down with new window size, ensure it is at least 1
    max_grid = max(int(round(30/windowsize)), 1) #scale 30 (750km) down with new window size, ensure it is at least 1

    min_time = max(int(round(min_outlier_timesize/time_resolution)),1)
    max_time = max(int(round(max_outlier_timesize/time_resolution)),1) 

  #open outputfile
  f = open(outputfile,'w')
  
  ##### LIBMAXDIV PARAMS
  #Turn into np.ndarray
  if univar:
    if not time_only:
      data_regrid = data_regrid.expand_dims({"height":1,"variables":1},axis=(3,4)) #add dummy dimensions to match mdi required input dimensions
    npar = data_regrid[variable].values
  
  #MANUALLY CHANGE THESE TO MATCH WHEN HANDLING MULTIVAR FILES
  else:
    if not time_only:
      data_regrid = data_regrid.expand_dims({"height":1},axis=(3))

    #Match these to the Xarray variable names
    #ZUIJLEN SETTINGS:
    SPV = data_regrid.SPV.values
    WON = data_regrid.WON.values
    WOF = data_regrid.WOF.values
    #ZUIJLEN SETTINGS FOR Z SCORES:
    # SPV_Z = data_regrid.SPV_Z.values
    # WON_Z = data_regrid.WON_Z.values
    # WOF_Z = data_regrid.WOF_Z.values

    if time_only:
      #ZUIJLEN SETTINGS:
      npar = np.stack((SPV, WON, WOF),axis=0)
      #Z SCORE :
      #npar = np.stack((SPV_Z, WON_Z, WOF_Z),axis=0)

    else:
      #ZUIJLEN SETTINGS:
      npar = np.stack((SPV, WON, WOF),axis=4) 
      #Z SCORE
      #npar = np.stack((SPV_Z, WON_Z, WOF_Z),axis=4) 

  #logging
  print(data_regrid)
  print(npar.shape)
  print(npar.shape, file=f)

  #number of returned intervals.
  num_intervals=50

  if time_only:
    extint_min_len = time_only_min_range
    extint_max_len = time_only_max_range
  else:
    extint_min_len = [min_time,min_grid,min_grid,1]
    extint_max_len = [max_time,max_grid,max_grid,1]
  preproc = preproc

  #Temporal Context Embedding
  #time-dimension params:
  td_lag = 8 #Capital T in literature, delay parameter
  td_dim = 4 #Capital K in literature. Indicates the number of timesteps to use (k-1 steps will be used)

  #spatial neighbor embedding info can be found in preproc.h
  #Set kx,ky or kz to 1 disables embedding along that axis
  #dt is the delay, set to 1 for direct neighbors
  kx=1
  ky=1
  kz=1
  dx=1
  dy=1
  dz=1

  #start timer
  start = time.perf_counter() 

  #Call the MDI function from the libmaxdiv_wrapper
  result = libmaxdiv_wrapper.maxdiv(npar,method=method,mode=mode,num_intervals=num_intervals,proposals=proposals,extint_min_len=extint_min_len, 
                                    extint_max_len=extint_max_len,preproc=preproc,td_lag=td_lag,td_dim=td_dim,kx=kx,ky=ky,kz=kz,dx=dx,dy=dy,dz=dz) 
  stop = time.perf_counter()

  #Output
  print("Detected intervals:",file=f)
  print(result,file=f)
  print("#####################################",file=f)
  print(f"time spent on mdi:  {stop - start:0.2f}",file=f)
  print("Used param settings:",file=f)
  print(f"method = {method}, mode = {mode}, num_intervals = {num_intervals}, proposals = {proposals}, extint_min_len = {extint_min_len},"  
        f"extint_max_len = {extint_max_len}, preproc = {preproc}, td_lag={td_lag}, td_dim = {td_dim}," 
        f"kx = {kx}, ky = {ky}, kz = {kz}, dx = {dx}, dy = {dy}, dz = {dz}, min_temporal_length = {min_grid}, max_temporal_length={max_grid}",file=f)
  print("data used:",file=f)
  
  print(f"{fileglob}, timeslice = {time_slice}, region = longitude={lon_slice},latitude={lat_slice}, windowsize = {windowsize}, timeresolution = {time_resolution}, univar ={univar}, variable= {variable}",file=f)
  f.close()


################## Example calc_mdi runs used for the experiments. Everything below can be removed, but are left as examples of how to use calc_mdi.

#Spatial run
#calc_mdi(data_small,"output/param_tuning/spatial/2010-2019_res15h4_hot_KDE_TS",windowsize =6, time_only=False, time_resolution=4, proposals='hotellings_t',mode='TS', method='parzen', preproc=["normalize"], univar=True, variable="WON", min_outlier_timesize = 24, max_outlier_timesize=24*3)

#calc_mdi(data_small,"output/param_tuning/spatial/2010-2019_res15h4_hot_KDE_CE",windowsize = 6, time_only=False, time_resolution=4, proposals='hotellings_t',mode='TS', method='CROSSENT', preproc=["normalize"], univar=True, variable="WON", min_outlier_timesize = 24, max_outlier_timesize=24*3)

###### DECADES RUN. THIS WAS USED FOR THE CLIMATE CHANGE EXPERIMENTS
# fiftees = (slice("1950","1959"), "fiftees")
# sixtees = (slice("1960","1969"), "sixtees")
# seventees = (slice("1970","1979"), "seventees")
# eightees = (slice("1980","1989"), "eightees")
# ninetees = (slice("1990","1999"),"ninetees")
# zeroes = (slice("2000","2009"), "zeroes")
# tens = (slice("2010","2019"), "tens")

# decades = [fiftees,sixtees,seventees,eightees,ninetees,zeroes,tens]

# for decade in decades:
#   print(f"Currently calculating decade {decade[1]}")
#   days = range (2,10)
#   data_decade = data_timeslice.sel(time=decade[0])
#   data_decade["TOTAL"] = data_decade["WON"] + data_decade["WOF"] + data_decade["SPV"]
#   for day in days:
#     if day == 9:
#       calc_mdi(data_decade,f"output/final_experiments/time_only/full_region/ERA5_summed/decades/{decade[1]}/{decade[1]}_hotteling_time1h_{day * 24}min_{(day + 1) * 24 }max_KDE_CrossEnt_normalized.log",windowsize = 1, time_only=True, time_resolution=1, proposals='hotellings_t', mode='CROSSENT', method='parzen', univar=True, variable="TOTAL", preproc=['normalize'], min_outlier_timesize = day*24, max_outlier_timesize=(day + 1) * 24)
#       calc_mdi(data_decade,f"output/final_experiments/time_only/full_region/ERA5_summed/decades/{decade[1]}/{decade[1]}_hotteling_time1h_{day * 24}min_{(day + 1) * 24 }max_KDE_TS_normalized.log",windowsize = 1, time_only=True, time_resolution=1, proposals='hotellings_t', mode='TS', method='parzen', univar=True, variable="TOTAL", preproc=['normalize'], min_outlier_timesize = day*24, max_outlier_timesize=(day + 1) * 24)


#     else:
#       calc_mdi(data_decade,f"output/final_experiments/time_only/full_region/ERA5_summed/decades/{decade[1]}/{decade[1]}_hotteling_time1h_{day * 24}min_{(day + 1) * 24 - 1}max_KDE_CrossEnt_normalized.log",windowsize = 1, time_only=True, time_resolution=1, proposals='hotellings_t', mode='CROSSENT', method='parzen', univar=True, variable="TOTAL", preproc=['normalize'], min_outlier_timesize = day*24, max_outlier_timesize=(day + 1) * 24 - 1)
#       calc_mdi(data_decade,f"output/final_experiments/time_only/full_region/ERA5_summed/decades/{decade[1]}/{decade[1]}_hotteling_time1h_{day * 24}min_{(day + 1) * 24 - 1}max_KDE_TS_normalized.log",windowsize = 1, time_only=True, time_resolution=1, proposals='hotellings_t', mode='TS', method='parzen', univar=True, variable="TOTAL", preproc=['normalize'], min_outlier_timesize = day*24, max_outlier_timesize=(day + 1) * 24 - 1)


## Cluster runs
# days = range (2,10)

# for day in days:
#   #If days are 8,9,10 do Unbiased KL, otherwise CE
#   if day == 9:
#     calc_mdi(data_timeslice,f"output/final_experiments/time_only/french_mixed_cluster/ERA5_summed/frmix_hotteling_time1h_{day * 24}min_{(day + 1) * 24 }max_KDE_TS_normalized.log",windowsize = 1, time_only=True, time_resolution=1, proposals='hotellings_t', mode='TS', method='parzen', univar=False, preproc=['normalize'], min_outlier_timesize = day*24, max_outlier_timesize=(day + 1) * 24)

#   if day == 7 or day == 8:
#     calc_mdi(data_timeslice,f"output/final_experiments/time_only/french_mixed_cluster/ERA5_summed/frmix_hotteling_time1h_{day * 24}min_{(day + 1) * 24 - 1}max_KDE_TS_normalized.log",windowsize = 1, time_only=True, time_resolution=1, proposals='hotellings_t', mode='TS', method='parzen', univar=False, preproc=['normalize'], min_outlier_timesize = day*24, max_outlier_timesize=(day + 1) * 24 - 1)


#   if day < 7:
#     calc_mdi(data_timeslice,f"output/final_experiments/time_only/french_mixed_cluster/ERA5_summed/frmix_hotteling_time1h_{day * 24}min_{(day + 1) * 24 - 1}max_KDE_CrossEnt_normalized.log",windowsize = 1, time_only=True, time_resolution=1, proposals='hotellings_t', mode='CROSSENT', method='parzen', univar=False, preproc=['normalize'], min_outlier_timesize = day*24, max_outlier_timesize=(day + 1) * 24 - 1)
    
    

###### Spatial location runs
# fiftees = (slice("1950","1959"), "fiftees")
# sixtees = (slice("1960","1969"), "sixtees")
# seventees = (slice("1970","1979"), "seventees")
# eightees = (slice("1980","1989"), "eightees")
# ninetees = (slice("1990","1999"),"ninetees")
# zeroes = (slice("2000","2009"), "zeroes")
# tens = (slice("2010","2019"), "tens")

# decades = [tens]

# for decade in decades:
#   print(f"Currently calculating decade {decade[1]}")
#   days = range (2,7)
#   data_decade = data_small.sel(time=decade[0])
#   data_decade["TOTAL"] = data_decade["WON"] + data_decade["WOF"] + data_decade["SPV"]
#   for day in days:
#     if day == 7:
#       calc_mdi(data_decade,f"output/final_experiments/spatial_temporal/full_region/decades/{decade[1]}/{decade[1]}_hotteling_time2h_{day * 24}min_{(day + 1) * 24 }max_Gauss_CrossEnt_normalized.log",windowsize = 2, time_only=False, time_resolution=2, proposals='hotellings_t', mode='CROSSENT', method='gaussian_cov', univar=True, variable="TOTAL", preproc=['normalize'], min_outlier_timesize = day*24, max_outlier_timesize=(day + 1) * 24)
#       calc_mdi(data_decade,f"output/final_experiments/spatial_temporal/full_region/decades/{decade[1]}/{decade[1]}_hotteling_time2h_{day * 24}min_{(day + 1) * 24 }max_Gauss_TS_normalized.log",windowsize = 2, time_only=False, time_resolution=2, proposals='hotellings_t', mode='TS', method='gaussian_cov', univar=True, variable="TOTAL", preproc=['normalize'], min_outlier_timesize = day*24, max_outlier_timesize=(day + 1) * 24)


#     else:
#       calc_mdi(data_decade,f"output/final_experiments/spatial_temporal/full_region/decades/{decade[1]}/{decade[1]}_hotteling_time2h_{day * 24}min_{(day + 1) * 24 - 1}max_Gauss_CrossEnt_normalized.log",windowsize = 2, time_only=False, time_resolution=2, proposals='hotellings_t', mode='CROSSENT', method='gaussian_cov', univar=True, variable="TOTAL", preproc=['normalize'], min_outlier_timesize = day*24, max_outlier_timesize=(day + 1) * 24 - 1)
#       calc_mdi(data_decade,f"output/final_experiments/spatial_temporal/full_region/decades/{decade[1]}/{decade[1]}_hotteling_time2h_{day * 24}min_{(day + 1) * 24 - 1}max_Gauss_TS_normalized.log",windowsize = 2, time_only=False, time_resolution=2, proposals='hotellings_t', mode='TS', method='gaussian_cov', univar=True, variable="TOTAL", preproc=['normalize'], min_outlier_timesize = day*24, max_outlier_timesize=(day + 1) * 24 - 1)
